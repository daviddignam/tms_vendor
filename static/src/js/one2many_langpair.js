odoo.define('tms_vendor.one2many_langpair', function (require) {
	"use strict";
	
	var core = require('web.core');
	var ListView = require('web.ListView');
	var Model = require('web.Model');
	var session = require('web.session');
	var QWeb = core.qweb;
	var list_widget_registry = core.list_widget_registry;
	
	//Add to base ListView Widget to include case for our particular widget in render_cell
	//this will be used to execute any rpc calls and set up our widget before rendering
	ListView.List.include({
		 render_cell: function (record, column)
		{
			if(column.widget == 'one2many_langpair')
			{
				var spoken = record.get(column.id);
				if (spoken != ""){
					//retrieve vendor language data from backend
					var deferred = new jQuery.Deferred(),self = this, record;
					var vendor_id = record.get('id');
					var tms_vendor_languages = new Model('tms.vendor.languages');
					self.html_value = "";
					tms_vendor_languages.query(['id','source','target','is_native'])
						.filter([['vendor','=',vendor_id]])
						.all().then(function (lang_pairs){
							//format each language pair as it's own span with CSS to distinguish if this pair includes their native language
							_.each(lang_pairs, function(lang_pair)
							{
								if (lang_pair.is_native){
									self.html_value += "<span class='oe_one2many_lang_pair_native'>" + lang_pair.source[1] + "&gt;" + lang_pair.target[1] + "</span>";
								}else{
									self.html_value += "<span class='oe_one2many_lang_pair_normal'>" + lang_pair.source[1] + "&gt;" + lang_pair.target[1] + "</span>";
								}
							});
							//set formatted value on record to be used in widgets _format later
							record.set(column.id + '__display', self.html_value);
							deferred.resolve();
						});
				}
			}
			return this._super(record, column);
		},
	});
	
	//Inherit and Extend ListView to override _format method for this particular widget to render it
	var langPairFieldOne2many = ListView.Column.extend({
		
		_format: function (row_data, options) {
			//if cell contains a value and the formatted value is present in the record change row_data to previously formatted value
			if(!_.isEmpty(row_data[this.id].value) && row_data[this.id + '__display'])
			{
				row_data[this.id] = row_data[this.id + '__display']
				return _.template(row_data[this.id].value)({});
			}
			return this._super(row_data, options);
			
		},
		
	});
	//add defined widget to ListView registry to allow it to be used within Odoo list/tree views
	core.list_widget_registry.add('field.one2many_langpair', langPairFieldOne2many);
	//return defined widget object to make them extensible/usable by other javascript widgets
	return {
		langPairFieldOne2many: langPairFieldOne2many,
	}
});