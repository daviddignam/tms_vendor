odoo.define('tms_vendor.many2one_vendor_rating', function(require)
{
	"use strict";
	var core = require('web.core');
	var form_common = require('web.form_common');
	var Model = require('web.Model')
	var ListView = require('web.ListView');
	
	//Add to base ListView Widget to include case for our particular widget in render_cell
	//this will be used to execute any rpc calls and set up our widget before rendering
	ListView.List.include({
		 render_cell: function (record, column)
		{
			if(column.widget == 'many2one_vendor_rating')
			{
				console.log("column: " + record.get(column.id));
				var rating = record.get(column.id);
				console.log("id: " + rating[0]);
				if (rating) {
					//retrieve vendor_rating data from backend
					var deferred = new jQuery.Deferred(),self = this, record;
					var tms_vendor_rating = new Model('tms.vendor.rating');
					tms_vendor_rating.query(['id','grade','percentage'])
						.filter([['id','=',rating[0]]])
						.all().then(function (vendor_rating){
							var rating_value = vendor_rating[0];
							//format vendor_rating for display in format "Letter Grade (%)"
							var cssClass = "oe_many2one_vendor_grade_" + rating_value.grade + " oe_vendor_grade_list";
							var rating_display = "<div class='" + cssClass + "'>" + rating_value.grade.toUpperCase() + " (" + rating_value.percentage + "%)</div>";
							//set formatted value on record to be used in widgets _format later
							record.set(column.id + '__display', rating_display);
							deferred.resolve();
						});
				}
				
			}
			return this._super(record, column);
		},
	});
	
	//Inherit and Extend ListView to override _format method for this particular widget to render it
	var vendorRatingListMany2one = ListView.Column.extend({
		_format: function (row_data, options) {
			//if cell contains a value and the formatted value is present in the record change row_data to previously formatted value
			if(!_.isEmpty(row_data[this.id].value) && row_data[this.id + '__display'])
			{
				row_data[this.id] = row_data[this.id + '__display']
				
				return _.template(row_data[this.id].value)({});
			}
			return this._super(row_data, options);
		},
	});
	
	
	//Form Widget
	var vendorRatingFieldMany2one = form_common.AbstractField.extend(form_common.ReinitializeFieldMixin,{
		
		query_value: function() {
			var self = this;
			var rating_id = this.field_manager.get_field_value("translator_rating")[0]

			var deferred = new jQuery.Deferred(),self = this;
			self.rating_value = false;
			//if cell contains value
			if (rating_id) {
				//retrieve vendor_rating record from backend
				var tms_vendor_rating = new Model('tms.vendor.rating');
				tms_vendor_rating.query(['id','grade','percentage'])
						.filter([['id','=',rating_id]])
						.all().then(function (vendor_rating){
						//set value on self so it is accessible by render_value
						self.rating_value = vendor_rating[0];
						deferred.resolve();
						self.render_value();
					});
			}else{
				deferred.resolve();
				self.render_value();
			}
			
			return jQuery.when(
								this.rating_value,
								deferred,
							);
		},
		
		init: function(field_manager, node)
		{
			this._super(field_manager, node);
			this.rating_value = false;
			//attach data retrieval method to load_record event so that data is refreshed and re-rendered every time a new record is loaded
			this._ic_field_manager.on("load_record", this, this.query_value)
		},
		
		start: function() {
			var self = this;
			this._super.apply(this, arguments);
		},
		
		
		
		render_value: function() {
			this._super();
			if (this.rating_value)
			{
				//if cell has value format it
				var vendor_rating = this.rating_value;
				var cssClass = "oe_many2one_vendor_grade_" + vendor_rating.grade;
				var rating_display = "<span class='" + cssClass + "'>" + vendor_rating.grade.toUpperCase() + " (" + vendor_rating.percentage + "%)</span>";
				//set widget elements html value to our formatted value to render it on the page
				this.$el.html(rating_display);
			} else {
				var rating_display = "<span></span>";
				this.$el.html(rating_display);
			}
		},
		
	});
	//add defined widgets to their respective registries to allow them to be used within Odoo views
	core.form_widget_registry.add('many2one_vendor_rating', vendorRatingFieldMany2one);
	core.list_widget_registry.add('field.many2one_vendor_rating', vendorRatingListMany2one);
	
	//return defined widget objects to make them extensible/usable by other javascript widgets
	return {
		vendorRatingFieldMany2one: vendorRatingFieldMany2one,
		vendorRatingListMany2one: vendorRatingListMany2one,
	}
});