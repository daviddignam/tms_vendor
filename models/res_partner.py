# -*- coding: utf-8 -*-

from openerp import models, fields, api
import logging
import datetime

_logger = logging.getLogger(__name__)

class tms_vendor(models.Model):
	_inherit = 'res.partner'
	
	#Fields for viewing Purchase Orders belong to vendor
	# NOTE: Tony
	x_po_ids = fields.Many2many(comodel_name="account.invoice", string="Purchase Orders", compute="get_purchase_orders", readonly="true", copy=False)
	x_po_total_amount = fields.Monetary(string='Total amount of pos', compute="get_purchase_orders")
	currency_id = fields.Many2one('res.currency', string='Currency', required=True, default=lambda self: self.env.user.company_id.currency_id)
	
	#Fields for viewing tasks assigned to vendor
	# x_task_ids = fields.One2many(string='Tasks by x_translator_id', comodel_name='project.task', inverse_name='x_translator')
	# x_task_count = fields.Integer(string='# Tasks by x_translator', compute='_compute_task_count')
	
	#Fields for recording languages vendor speaks
	# x_native = fields.Many2one(string='Native Language', comodel_name='tms.languages')
	# x_second_lang = fields.Many2one(string='Second Language', comodel_name='tms.languages')
	# x_third_lang = fields.Many2one(string='Third Language', comodel_name='tms.languages')
	# x_fourth_lang = fields.Many2one(string='Fourth Language', comodel_name='tms.languages')
	
	# x_language_pair = fields.One2many(string='Language Pairs', comodel_name='x_res.languages.pairs', inverse_name='x_translator')
	
	#Field for recording SAGE account for customer 
	# TODO: Move to separate addon for customer info (tms_sale)
	x_legacy_account = fields.Char(string='SAGE Account')

	x_specialisation = fields.Text(string='Specialisation')
	
	#Translator Status
	# TODO: Add proper Key Performance Indicator style rating system to replace this field
	x_status = fields.Selection(selection=[('good','Green: 1. Consistent Positive Feedback 2. Trusted Translator'),('acceptable','Yellow:  1. Expensive 2. Limited Availability 3. Problems with Formatting Documents 4. Late Delivery'),('poor','Orange: 1. Non-Professional Behaviour 2. Poor Quality Translations'),('unavailable','Pink: 1. Currently on holidays/temporarily inactive.'),('blacklist','Red: 1. Blacklisted - Do Not Use'),('unused','Grey: Has not been used in quite some time i.e. over a year.')], string='Translator Status')
	
	x_timezone = fields.Selection(selection=[('GMT+0','[GMT +0]'),('GMT+1','[GMT +1]'),('GMT+2','[GMT +2]'),('GMT+3','[GMT +3]'),('GMT+4','[GMT +4]'),('GMT+5','[GMT +5]'),('GMT+6','[GMT +6]'),('GMT+7','[GMT +7]'),('GMT+8','[GMT +8]'),('GMT+9','[GMT +9]'),('GMT+10','[GMT +10]'),('GMT+11','[GMT +11]'),('GMT+12','[GMT +12]'),('GMT-11','[GMT -11]'),('GMT-10','[GMT -10]'),('GMT-9','[GMT -9]'),('GMT-8','[GMT -8]'),('GMT-7','[GMT -7]'),('GMT-6','[GMT -6]'),('GMT-5','[GMT -5]'),('GMT-4','[GMT -4]'),('GMT-3','[GMT -3]'),('GMT-2','[GMT -2]'),('GMT-1','[GMT -1]')], string='Time Zone')
	x_training_notes = fields.Text(string='Notes on Training')
	
	x_primary_cat = fields.Many2one(string='Primary CAT Tool', comodel_name='x_res.cat')
	x_other_cat = fields.Text(string='Other CAT Tools')
	
	x_rate_per_word = fields.Float(string='Rate Per Word')
	x_rate_info = fields.Text(string='Rate Information')
	
	#Availability fields:- can translator be assigned to jobs
	availability_history = fields.One2many(comodel_name='tms.vendor.availability_history', inverse_name='vendor')
	available = fields.Boolean(compute="_calc_available", store=True)
	availability = fields.Selection([('available','Available'),('unavailable','Unavailable'),('blacklisted','Blacklisted')])
	reason = fields.Text()
	unavailable_from = fields.Datetime()
	unavailable_to = fields.Datetime()
	
	#Rating field: - how good is the translator
	translator_rating = fields.Many2one(comodel_name='tms.vendor.rating')
	rating_history = fields.One2many(comodel_name='tms.vendor.rating.history', inverse_name='vendor')
	
	#Fields used to notify users to update information
	update_req_availability = fields.Boolean()
	update_req_languages = fields.Boolean()
	update_req_rating = fields.Boolean(compute="_check_rating_req")
	
	#Customer Department
	# TODO: Move to addon dealing with proper Company/Department Heirarchy
	department = fields.Char(help="Department of Parent Company	")
	
	translatable_languages = fields.One2many(comodel_name='tms.vendor.languages', inverse_name='vendor')
	
	@api.depends('translator_rating')
	def _check_rating_req(self):
		for record in self:
			if record.supplier:
				if not record.translator_rating:
					record.update_req_rating = True
				else:
					record.update_req_rating = False
	
	@api.depends('availability')
	def _calc_available(self):
		for record in self:
			if record.supplier:
				if record.availability == 'available':
					record.update_req_availability = False
					record.available = True
				# if supplier has been marked as unavailable but the unavailable from date has yet to pass 
				# we don't want to mark translator as unavailable until unavailable_from date has passed
				elif record.availability == 'unavailable' and record.unavailable_from > str(datetime.datetime.now()) or record.unavailable_to < str(datetime.datetime.now()):
					record.update_req_availability = False
					record.available = True
				elif record.availability == False:
					record.update_req_availability = True
					record.available = False
				else:
					record.update_req_availability = False
					record.available = False

	@api.model
	def _cron_migrate_old_data(self):
		try:
			vendors = self.env['res.partner'].search([('translatable_languages','=',False),('supplier','=',True),('id','=',432)])
			_logger.debug("number of vendors without languages: " + str(len(vendors)))
			for vendor in vendors:
				if vendor.x_native and vendor.x_second_lang:
					self._create_tms_lang_pair(vendor, vendor.x_native, vendor.x_second_lang, native=True, bi_t=True, bi_p=True)
				if vendor.x_native and vendor.x_third_lang:
					self._create_tms_lang_pair(vendor, vendor.x_native, vendor.x_third_lang, native=True, bi_t=True, bi_p=True)
				if vendor.x_native and vendor.x_fourth_lang:
					self._create_tms_lang_pair(vendor, vendor.x_native, vendor.x_fourth_lang, native=True, bi_t=True, bi_p=True)
				if vendor.x_second_lang and vendor.x_third_lang:
					self._create_tms_lang_pair(vendor, vendor.x_second_lang, vendor.x_third_lang, native=False, bi_t=False, bi_p=False)
				if vendor.x_second_lang and vendor.x_fourth_lang:
					self._create_tms_lang_pair(vendor, vendor.x_second_lang, vendor.x_fourth_lang, native=False, bi_t=False, bi_p=False)
				vendor.update_req_languages = True
		except Exception:
			_logger.error("Error migrating res_partner data to tms_vendor", exc_info=True)
				
	def _create_tms_lang_pair(self, vendor, src, trg, native=False, bi_t=False, bi_p=False):
		_logger.debug("vendor: " + str(vendor.name))
		data = {
			'vendor': vendor.id,
			'is_native': native,
			'source': src.id,
			'target': trg.id,
		}
		if bi_t:
			data['translate_bilingual'] = True
		else:
			data['translate'] = True
		if bi_p:
			data['proofread_bilingual'] = True
		else:
			data['proofread'] = True
		vendor.translatable_languages.create(data)
	
	@api.model
	def _cron_infer_update_required(self):
		try:
			vendors_availability = self.env['res.partner'].search([('supplier','=',True)])
			for vendor in vendors:
				if not vendor.availability:
					vendor.update_req_availability = True
				if not vendor.translator_rating:
					vendor.update_req_rating = True
			
		except Exception:
			_logger.error("Error infering update status for Vendor", exc_info=True)
	
	@api.multi
	def action_change_availability(self):
		self.ensure_one()
		ctx = dict()
		ctx.update({
			'default_vendor': self.id,
		})
		action = self.env.ref('tms_vendor.action_set_availability')
		vals = action.read()[0]
		vals['context'] = ctx
		return vals
	
	@api.multi
	def action_set_initial_rating(self):
		self.ensure_one()
		ctx = dict()
		ctx.update({
			'default_vendor': self.id,
		})
		action = self.env.ref('tms_vendor.action_set_initial_rating')
		vals = action.read()[0]
		vals['context'] = ctx
		return vals

	@api.multi
	def _compute_task_count(self):
		try:
			for partner in self:
				tasks = self.env['project.task'].search([('x_translator', '=', partner.id)])
				_logger.debug('tasks count: '+str(tasks))
				partner.x_task_count = len(tasks)
		except Exception as e:
			_logger.debug('Error in the _compute_task_count: ', exc_info=True)
	
	@api.multi
	def get_purchase_orders(self):
		for partner in self:
			try:
				po_ids = self.env['account.invoice'].search([('partner_id','=', partner.id), ('type','=','in_invoice'), ('state', '!=', 'draft')])
				po_refund_ids = self.env['account.invoice'].browse()

				if po_ids:
					po_refund_ids = po_refund_ids.search([('type', '=', 'in_refund'), ('origin', 'in', po_ids.mapped('number')), ('origin', '!=', False)])

				total_po_amounts = sum([amount.amount_total for amount in po_ids])
				total_po_refund_amounts = sum([amount.amount_total for amount in po_refund_ids])

				partner.update({
					'x_po_total_amount': total_po_amounts + total_po_refund_amounts,
					'x_po_ids': po_ids + po_refund_ids
				})
			except Exception:
				_logger.debug("error getting pos" + str(total_po_amounts) + str(partner), exc_info=True)
	
	@api.multi
	def action_open_availability_history(self):
		try:
			action = self.env.ref('tms_vendor.action_availability_history_tree')
			vals = action.read()[0]
			vals['domain'] = [('id','in',self.availability_history.ids)]
			return vals
		except Exception:
			_logger.error("Error opening availability history", exc_info=True)
			
	@api.multi
	def action_open_rating_history(self):
		try:
			action = self.env.ref('tms_vendor.action_rating_history_tree')
			vals = action.read()[0]
			vals['domain'] = [('id','in',self.rating_history.ids)]
			return vals
		except Exception:
			_logger.debug("Error opening rating history", exc_info=True)
	
	
	@api.model
	def _cron_end_unavailable_period(self):
			try:
				vendors = self.env['res.partner'].search([('supplier','=',True),('availability','=','unavailable')])
				for vendor in vendors:
					if vendor.unavailable_to < str(datetime.datetime.now()):
						data = {
							'availability': 'available',
							'reason': 'Marked available by system as unavailable_to date has passed.',
							'unavailable_from': False,
							'unavailable_to': False,
						}
						complete = vendor.write(data)
						if complete:
							history_data = {
								'vendor': vendor.id,
								'status': vendor.availability,
								'reason': vendor.reason,
								'unavailable_from': vendor.unavailable_from,
								'unavailable_to': vendor.unavailable_to,
								'set_by': self.env.user.id
							}
							vendor.availability_history.create(history_data)
					elif vendor.available and vendor.unavailable_to > str(datetime.datetime.now()) and vendor.unavailable_from < str(datetime.datetime.now()):
						vendor._calc_available()
			except Exception:
				_logger.error("Error automatically ending vendor unavailability period.", exc_info=True)
				
	def _load_availability_wizard(self, cr, uid, ids, context=None):
		try:
			#get vendor records for availability update
			vendors = self.browse(cr, uid, ids)
			#create record of wizard to attach vendors to
			wizard  = self.pool['tms.vendor.availability_update.wizard'].create(cr, uid, {})
			for vendor in vendors:
				#add record for each vendor to wizard to allow wizard to update availability fields of each vendor later
				data = {
					'vendor': vendor.id,
					'wizard': wizard,
					'unavailable_from': False,
					'unavailable_to': False,
					'reason': '',
				}
				self.pool['tms.vendor.availability_update.unavailable_row'].create(cr, uid, data)
			#get view_id for wizard and open view for the record we just created to allow user to update availability
			view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'tms_vendor','view_update_availability_wizard')[1]
			return {
				'name': 'Update Vendor Availability',
				'view_type': 'form',
				'view_mode': 'form',
				'res_model': 'tms.vendor.availability_update.wizard',
				'res_id': wizard,
				'view_id': view_id,
				'type': 'ir.actions.act_window',
				'target': 'new',
			}
		except Exception:
			_logger.error("Error loading Availability Update Wizard", exc_info=True)
	
class tms_vendor_languages(models.Model):
	_name = 'tms.vendor.languages'
	
	vendor = fields.Many2one(comodel_name='res.partner')
	source = fields.Many2one(comodel_name='tms.languages')
	target = fields.Many2one(comodel_name='tms.languages')
	is_native = fields.Boolean(help="Signifies that the Source Language is their Native Language")
	translate = fields.Boolean(help="Signifies that Vendor Translates from Source to Target only for this language pair")
	translate_bilingual = fields.Boolean(help="Signifies that Vendor Translates Bilingually in this pair")
	proofread = fields.Boolean(help="Signifies that Vendor Proofreads from Source to Target only for this language pair")
	proofread_bilingual = fields.Boolean(help="Signifies that Vendor Proofreads Bilingually in this pair")
	
class tms_vendor_availability_history(models.Model):
	_name = 'tms.vendor.availability_history'
	
	set_by = fields.Many2one(comodel_name='res.users')
	vendor = fields.Many2one(comodel_name='res.partner')
	status = fields.Selection([('available','Available'),('unavailable','Unavailable'),('blacklisted','Blacklisted')])
	reason = fields.Text()
	unavailable_from = fields.Datetime()
	unavailable_to = fields.Datetime()

class tms_vendor_rating(models.Model):
	_name = 'tms.vendor.rating'
	
	vendor = fields.Many2one(comodel_name='res.partner')
	grade = fields.Selection([('a','A'),('b','B'),('c','C'),('d','D'),('e','E'),('f','F')],compute="_compute_grade")
	percentage = fields.Float()
	
	@api.depends('percentage')
	def _compute_grade(self):
		percentage = self.percentage
		if percentage <= 24:
			self.grade = 'f'
		elif percentage >= 25 and percentage <= 39:
			self.grade = 'e'
		elif percentage >= 40 and percentage <= 54:
			self.grade = 'd'
		elif percentage >= 55 and percentage <= 69:
			self.grade = 'c'
		elif percentage >= 70 and percentage <= 84:
			self.grade = 'b'
		elif percentage >= 85:
			self.grade = 'a'
	
class tms_vendor_rating_history(models.Model):
	_name = 'tms.vendor.rating.history'
	
	vendor = fields.Many2one(comodel_name='res.partner')
	rating_type = fields.Selection([('init','Initial'),('task','Completed Task')])
	set_by = fields.Many2one(comodel_name='res.users')
	
	overall_rating = fields.Float()
	rating_shift_contribution = fields.Char()
	
	#Initial Rating fields
	experience = fields.Selection([('none','Inexperienced ( < 6 months)'),('minor','Minor Experience (> 6 months < 4 years)'),('average','Experienced (4 - 5 years)'),('high','Highly Experienced (6 - 10 years)'),('consummate','Consummate (> 10 years)')],help="Years experience")
	qualification = fields.Selection([('none','None'),('cert','Certificate'),('dip','Diploma'),('bachelor','Bachelors Degree'),('master','Masters Degree'),('phd','PHD')], help="Highest relevant qualification (i.e. award in Translation field)")
	education = fields.Selection([('none','Below Secondary Level'),('secondary','Secondary (High) School'),('bachelor','Bachelors Degree or Equivalent(H. Dip)'),('master','Masters Degree'),('phd','PHD')],help="Highest level of Education attained")
	rate = fields.Selection([('cheap','Cheap'),('below','Below Average'),('average','Average'),('above','Above Average'),('expensive','Expensive')], help="How expensive the vendors desired rate is vs the average rates for their languages")
	test_result = fields.Integer(string="Translator Test Result", help="Result of the vendors completed translation test")
	
	#Task Rating fields
	task_type = fields.Selection([('translation','Translation'),('proofread','Proofread')], help="The type of task completed effects the percentage of the overall rating that is contributed to changing the vendors rating. (10% for translation and 5% for proofreading)")
	task_id = fields.Many2one(comodel_name='project.task')
	punctuality = fields.Selection([('5','5'),('4','4'),('3','3'),('2','2'),('1','1'),('0','0'),('-1','-1'),('-2','-2'),('-3','-3'),('-4','-4'),('-5','-5')], help="How well did the Vendor keep to the assigned deadline?")
	quality = fields.Selection([('5','5'),('4','4'),('3','3'),('2','2'),('1','1'),('0','0'),('-1','-1'),('-2','-2'),('-3','-3'),('-4','-4'),('-5','-5')], help="How would you rate the quality of the Vendors work?")
	responsiveness = fields.Selection([('5','5'),('4','4'),('3','3'),('2','2'),('1','1'),('0','0'),('-1','-1'),('-2','-2'),('-3','-3'),('-4','-4'),('-5','-5')], help="How quickly did the Vendor respond to queries/requests for update etc. while completing the task.")
	ease_of_use = fields.Selection([('5','5'),('4','4'),('3','3'),('2','2'),('1','1'),('0','0'),('-1','-1'),('-2','-2'),('-3','-3'),('-4','-4'),('-5','-5')], help="How easy was the translator to deal with? ( e.g. politeness, flexibility, willingness to compromise etc.)")
	
	