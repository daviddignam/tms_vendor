# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)


class TmsVendorAvailabilityWizard(models.TransientModel):
	_name = 'wizard.tms.vendor.availability'
	
	vendor = fields.Many2one(comodel_name='res.partner')
	status = fields.Selection([('available','Available'),('unavailable','Unavailable'),('blacklisted','Blacklisted')], string='Availability Status')
	reason = fields.Text()
	unavailable_from = fields.Datetime()
	unavailable_to = fields.Datetime()
	
	@api.multi
	def set_availability(self):
		vendor = self.env['res.partner'].browse(self.vendor.id)
		
		data = {
			'availability': self.status,
			'reason': self.reason,
			'unavailable_from': self.unavailable_from,
			'unavailable_to': self.unavailable_to,
		}
		complete = vendor.write(data)
		vendor._calc_available()
		_logger.debug("availability set: " + str(complete))
		if complete:
			history_data = {
				'vendor': vendor.id,
				'status': self.status,
				'reason': self.reason,
				'unavailable_from': self.unavailable_from,
				'unavailable_to': self.unavailable_to,
				'set_by': self.env.user.id
			}
			vendor.availability_history.create(history_data)