# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.exceptions import Warning, UserError, ValidationError
from datetime import datetime
import datetime
import logging

_logger = logging.getLogger(__name__)

class VendorTaskRatingWizard(models.TransientModel):
	_name = 'tms.vendor.task_rating.wizard'
	
	vendor = fields.Many2one(comodel_name='res.partner')
	task_type = fields.Selection([('translation','Translation'),('proofread','Proofread')], help="The type of task completed effects the percentage of the overall rating that is contributed to changing the vendors rating. (10% for translation and 5% for proofreading)")
	task_id = fields.Many2one(comodel_name='project.task')
	#punctuality weight: 25%
	punctuality = fields.Selection([('5','5'),('4','4'),('3','3'),('2','2'),('1','1'),('0','0'),('-1','-1'),('-2','-2'),('-3','-3'),('-4','-4'),('-5','-5')], help="How well did the Vendor keep to the assigned deadline?")
	#quality weight: 40%
	quality = fields.Selection([('5','5'),('4','4'),('3','3'),('2','2'),('1','1'),('0','0'),('-1','-1'),('-2','-2'),('-3','-3'),('-4','-4'),('-5','-5')], help="How would you rate the quality of the Vendors work?")
	#responsiveness weight: 15%
	responsiveness = fields.Selection([('5','5'),('4','4'),('3','3'),('2','2'),('1','1'),('0','0'),('-1','-1'),('-2','-2'),('-3','-3'),('-4','-4'),('-5','-5')], help="How quickly did the Vendor respond to queries/requests for update etc. while completing the task.")
	#Ease of Use weight: 20%
	ease_of_use = fields.Selection([('5','5'),('4','4'),('3','3'),('2','2'),('1','1'),('0','0'),('-1','-1'),('-2','-2'),('-3','-3'),('-4','-4'),('-5','-5')], help="How easy was the translator to deal with? ( e.g. politeness, flexibility, willingness to compromise etc.)")
	
	def _get_percentage_contribution(self, value, max_value, percent_weight):
		value = float(value)
		percentage = value / max_value
		weighted_percent = percentage * percent_weight
		return weighted_percent
	
	def _calc_overall_rating(self):
		punctuality = self._get_percentage_contribution(self.punctuality, 5.0, 0.25)
		quality = self._get_percentage_contribution(self.quality, 5.0, 0.4)
		responsiveness = self._get_percentage_contribution(self.responsiveness, 5.0, 0.15)
		ease_of_use = self._get_percentage_contribution(self.ease_of_use, 5.0, 0.2)
		overall_rating = punctuality + quality + responsiveness + ease_of_use
		return overall_rating
		
	@api.multi
	def modify_vendor_rating(self):
		overall_rating = self._calc_overall_rating()
		current_rating = self.vendor.translator_rating
		if task_type == 'translation':
			rating_contribution = overall_rating * 0.1
		else:
			rating_contribution = overall_rating * 0.05
		new_rating = current_rating + rating_contribution
		complete = self.vendor.write({'translator_rating':new_rating})
		if complete:
			self.record_history(overall_rating, rating_contribution)
		
	def record_history(self, total_rating, rating_contribution):
		data = {
				'vendor': self.vendor.id,
				'set_by': self.env.user.id,
				'rating_type': 'task',
				'task_type': self.task_type,
				'task_id': self.task_id.id,
				'punctuality': self.punctuality,
				'quality': self.quality,
				'responsiveness': self.responsiveness,
				'ease_of_use': self.ease_of_use,
				'overall_rating': total_rating,
				'rating_shift_contribution': str(rating_contribution),
			}
		
		self.vendor.rating_history.create(data)

class VendorInitRatingWizard(models.TransientModel):
	_name = 'tms.vendor.initial_rating.wizard'
	
	vendor = fields.Many2one(comodel_name='res.partner')
	#experience weight: 20%
	experience = fields.Selection([('none','Inexperienced ( < 6 months)'),('minor','Minor Experience (> 6 months < 4 years)'),('average','Experienced (4 - 5 years)'),('high','Highly Experienced (6 - 10 years)'),('consummate','Consummate (> 10 years)')],help="Years experience")
	#qualification weight: 10%
	qualification = fields.Selection([('none','None'),('cert','Certificate'),('dip','Diploma'),('bachelor','Bachelors Degree'),('master','Masters Degree'),('phd','PHD')], help="Highest relevant qualification (i.e. award in Translation field)")
	#education weight: 5%
	education = fields.Selection([('none','Below Secondary Level'),('secondary','Secondary (High) School'),('bachelor','Bachelors Degree or Equivalent(H. Dip)'),('master','Masters Degree'),('phd','PHD')],help="Highest level of Education attained")
	#rate weight: 25%
	rate = fields.Selection([('cheap','Cheap'),('below','Below Average'),('average','Average'),('above','Above Average'),('expensive','Expensive')], help="How expensive the vendors desired rate is vs the average rates for their languages")
	#test weight: 40%
	test_result = fields.Integer(string="Translator Test Result", help="Result of the vendors completed translation test")
	
	def _get_experience_rating(self):
		experience = self.experience
		value = 0.0
		max_value = 4.0
		experience_weight = 0.2
		if experience == 'minor':
			value = 1.0
		elif experience == 'average':
			value = 2.0
		elif experience == 'high':
			value = 3.0
		elif experience == 'consummate':
			value = 4.0
		
		weight_percent_rating = self._get_percentage_contribution(value, max_value, experience_weight)
		return weight_percent_rating
	
	def _get_qualification_rating(self):
		qualification = self.qualification
		value = 0.0
		max_value = 5.0
		qualification_weight = 0.1
		if qualification == 'cert':
			value = 1.0
		elif qualification == 'dip':
			value = 2.0
		elif qualification == 'bachelor':
			value = 3.0
		elif qualification == 'master':
			value = 4.0
		elif qualification == 'phd':
			value = 5.0
		
		weight_percent_rating = self._get_percentage_contribution(value, max_value, qualification_weight)
		return weight_percent_rating
	
	def _get_education_rating(self):
		education = self.education
		value = 0.0
		max_value = 4.0
		education_weight = 0.05
		if education == 'secondary':
			value = 1.0
		elif education == 'bachelor':
			value = 2.0
		elif education == 'master':
			value = 3.0
		elif education == 'phd':
			value = 4.0
		
		weight_percent_rating = self._get_percentage_contribution(value, max_value, education_weight)
		return weight_percent_rating
	
	def _get_rate_rating(self):
		rate = self.rate
		value = 1.0
		max_value = 5.0
		rate_weight = 0.25
		if rate == 'above':
			value = 2.0
		elif rate == 'average':
			value = 3.0
		elif rate == 'below':
			value = 4.0
		elif rate == 'cheap':
			value = 5.0
		
		weight_percent_rating = self._get_percentage_contribution(value, max_value, rate_weight)
		return weight_percent_rating
	
	def _get_test_result_rating(self):
		value = float(self.test_result)
		max_value = 100.0
		test_result_weight = 0.4
		
		weight_percent_rating = self._get_percentage_contribution(value, max_value, test_result_weight)
		return weight_percent_rating
	
	def _get_percentage_contribution(self, value, max_value, percent_weight):
		percentage = value / max_value
		weighted_percent = percentage * percent_weight
		return weighted_percent
		
	@api.constrains('test_result')
	def _validate_test_result(self):
		if self.test_result > 100 or self.test_result < 0:
			raise ValidationError("Translator Test Result must be between 0 and 100.")
	
	@api.multi
	def set_initial_rating(self):
		try:
			self.ensure_one()
			experience = self._get_experience_rating()
			qualification = self._get_qualification_rating()
			education = self._get_education_rating()
			rate = self._get_rate_rating()
			test_result = self._get_test_result_rating()
			total_rating = (experience + qualification + education + rate + test_result) * 100
			_logger.debug("total_rating: " + str(total_rating))
			vendor_rating = self.vendor.translator_rating.create({'vendor':self.vendor.id, 'percentage': total_rating})
			complete = self.vendor.write({'translator_rating': vendor_rating.id})
			if complete:
				self.record_history(total_rating)
		except Exception:
			_logger.error("Error setting initial rating", exc_info=True)
			
	def record_history(self, total_rating):
		data = {
				'vendor': self.vendor.id,
				'set_by': self.env.user.id,
				'rating_type': 'init',
				'experience': self.experience,
				'qualification': self.qualification,
				'education': self.education,
				'rate': self.rate,
				'test_result': self.test_result,
				'overall_rating': total_rating,
				'rating_shift_contribution': "+" + str(total_rating),
			}
			
		self.vendor.rating_history.create(data)
		
		
			
		