# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.exceptions import Warning, UserError
from datetime import datetime
import datetime
import logging

_logger = logging.getLogger(__name__)

class VendorAvailabilityUpdateWizard(models.TransientModel):
	_name = "tms.vendor.availability_update.wizard"
	
	availability = fields.Selection([('available','Available'),('unavailable','Unavailable'),('blacklisted','Blacklisted')])
	vendor_rows = fields.One2many(comodel_name="tms.vendor.availability_update.unavailable_row", inverse_name="wizard")
	
	@api.onchange('availability')
	def onchange_availability(self):
		try:
			if self.availability == 'available':
				data = {
					'unavailable': False,
					'reason_required': False,
					'unavailable_from': False,
					'unavailable_to': False,
					'reason': '',
				}
				for vendor_row in self.vendor_rows:
					vendor_row.update(data)
			elif self.availability == 'unavailable':
				data = {
					'unavailable': True,
					'reason_required': True,
					'unavailable_from': False,
					'unavailable_to': False,
					'reason': '',
				}
				for vendor_row in self.vendor_rows:
					vendor_row.update(data)
			elif self.availability == 'blacklisted':
				data = {
					'unavailable': False,
					'reason_required': True,
					'unavailable_from': False,
					'unavailable_to': False,
					'reason': '',
				}
				for vendor_row in self.vendor_rows:
					vendor_row.update(data)
		except Exception:
			_logger.error("Error changing availability",exc_info=True)
	
	@api.multi
	def set_availability(self):
		try:
			for vendor_row in self.vendor_rows:
				data = {
					'availability': self.availability,
					'unavailable_from': vendor_row.unavailable_from,
					'unavailable_to': vendor_row.unavailable_to,
					'reason': vendor_row.reason,
				}
				vendor = vendor_row.vendor
				complete = vendor.write(data)
				vendor._calc_available()
				if complete:
					history_data = {
						'vendor': vendor.id,
						'status': self.availability,
						'reason': vendor_row.reason,
						'unavailable_from': vendor_row.unavailable_from,
						'unavailable_to': vendor_row.unavailable_to,
						'set_by': self.env.user.id
					}
					vendor.availability_history.create(history_data)
		except Exception:
			_logger.error("Error setting Vendor Availability", exc_info=True)


class VendorAvailabilityUpdateUnavailableRow(models.TransientModel):
	_name = "tms.vendor.availability_update.unavailable_row"
	
	vendor = fields.Many2one(comodel_name='res.partner')
	wizard = fields.Many2one(comodel_name='tms.vendor.availability_update.wizard')
	unavailable_from = fields.Datetime()
	unavailable_to = fields.Datetime()
	reason = fields.Text()
	unavailable = fields.Boolean()
	reason_required = fields.Boolean()