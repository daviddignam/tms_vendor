# -*- coding: utf-8 -*-
{
    'name': "tms_vendor",
	
	'summary': """
		Adds functionality for Vendor Management in relation to Translation Management System.
		TODO: Add Rating & Availability system for Vendors
		""",
	'author': "David Dignam",
	'website': "",
	
	'version': "1.0",
	
	'depends': ['tms_res', 'account', 'project', 'web'],
	
	'data': [
		# 'security/ir.model.access.csv',
		'views/res_partner.xml',
		'views/assets.xml',
		'wizard/availability_wizard.xml',
		'wizard/vendor_update_wizard.xml',
		'wizard/rating_wizard.xml',
		'views/availability_history.xml',
		'views/rating_history.xml',
		'views/vendor_update_view.xml',
		'data/auto_end_unavailability_cron.xml',
		'data/infer_vendor_update_required.xml',
		'data/one_off_migrate_data_from_res_partner.xml',
	],
	'demo': [
	],
	'qweb': [
	],
}